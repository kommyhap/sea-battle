#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "map.h"

#define DEFAULT_WORKERS_NUMBER 4

enum status {GETTING_MAP, GAMING, EXITING};
struct game
{
	int status;
	int sockets[2];
	struct ship ships[2][SHIPSNUM];
	int current_client;
	void* buffer;
	ssize_t buffer_size;
}

/*
 * Элемент ipc очереди.
 */
struct mqitem
{
	long type;
	struct game *game;
}

struct worker_args
{
	int mqid;
}

struct thread_data
{
	struct game *games;
	size_t size;
}

struct buf_shot
{
	union {
		struct shot *shot;
		void *buf;
	}
	ssize_t size;
}

int connector_socket;

struct thread_data * threads_data;

/*
 * Находит структуру из массива game_list, содержащую нужный сокет, возвращает
 *   указатель на искомую структуру. В случае ошибки вернёт NULL.
 */
struct game* find_game_by_socket(int socket, const struct game *game_list,
		size_t size);

/*
 * Отправляет "выстрел" обоим игрокам, одному необходим код для того, чтобы
 *   узнать, попал он или нет, другому - факт выстрела +результат.
 * В случае ошибки возвращает код ошибки.
 */
ssize_t send_shot(const struct game *game, const struct shot *shot);

/*
 * Приём выстрела от игрока, не блокирует сокет, как и поток, не делает
 *   проверки на то, от того ли игрока мы ждали выстрела, заполняет структуру
 *   shot.
 * В случае, если выстрел был принят не полностью, возвращает 0, иначе 1.
 * В случае ошибки возвращает код ошибки.
 */
int get_shot(int socket, struct buf_shot *shot);

/*
 * Получение карты от конкретного игрока, заполняет структуру game. Не делает
 *   проверку на валидность карты.
 * В случае, если карта не была принята полностью, возвращает 0, иначе 1.
 * В случае ошибки возвращает код ошибки.
 */
int get_map(int socket, struct game *game);

/*
 * Проверяет карту на валидность. В случае, если карта валидна возвращает 1,
 *   иначе 0.
 */
int check_map(const struct ship ships[SHIPSNUM]);

/*
 * Подсчитывает число не раненых ячеек кораблей по карте игрока. Если вернёт 0,
 *   значит игрок проиграл.
 */
uint8_t get_alive(const struct ship ships[SHIPSNUM]);

/*
 * Отправляет победителю и проигравшему соответствующие сообщения.
 * В случае ошибки вернёт её код.
 */
int end_game(struct game *game);

void *worker(void*);

/*
 * Не придумал названия лучше. Инициализирует данные, запускает потоки, ловит
 *  сигналы и обрабатывает их, перезапускает потоки при падении, завершает их.
 * Если не удаётся перезапустить поток, его данные передаёт остальным для
 *   обработки.
 */
void main_daemon();

struct game *find_game_by_socket(int socket, const struct game *game_list,
		size_t size)
{
	for(size_t i = 0; i < size; ++i)
	{
		if(game_list[i].sockets[0] == socket ||
				game_list[1].sockets[1] == socket)
			return &game_list[i];
	}
	return NULL;
}

ssize_t send_shot(const struct game *game, const struct shot *shot)
{
	ssize_t ret = 0;
	ssize_t v = sizeof(*shot);
	while(v != 0)
	{
		ret = send(game->sockets[0], shot, sizeof(*shot), 0);
		if(ret < 0)
			return ret;
		v -= ret;
	}
	v = sizeof(*shot);
	while(v != 0)
	{
		ret = send(game->sockets[1], shot, sizeof(*shot), 0);
		if(ret < 0)
			return ret;
		v -= ret;
	}
	return 0;
}

int get_shot(int socket, struct buf_shot *shot)
{
	if(shot->buf == NULL)
		shot->buf = malloc(sizeof(*shot->shot));
	size_t ret = recv(socket, shot->buf + shot->size,
			sizeof(*shot->shot) - shot->size, 0);
	if(ret + shot->size == sizeof(*shot->shot))
	{
		//TODO: заполнение result
		return 1;
	}
	if(ret < 0)
		return ret;
	shot->size = ret;
	return 0;
}

int get_map(struct game *game)
{
	if(game->buffer == NULL)
		game->buffer = malloc(SHIPSNUM*sizeof(struct ship));
	ssize_t ret = recv(game->current_client, game->buffer +
				game->buffer_size,
			SHIPSNUM*sizeof(struct ship) - game->buffer_size);
	if(ret + game->size == SHIPSNUM*sizeof(struct ship))
	{
		if(game->current_client == game->sockets[0])
		{
			memcpy(&(game->ships[0]), game->buf,
					SHIPSNUM*sizeof(struct ship));
		}
		else
		{
			memcpy(&(game->ships[1]), game->buf,
					SHIPSNUM*sizeof(struct ship))
		}
		return 1;
	}
	if(ret < 0)
		return ret;
	gsme->buffer_size = ret;
	return 0;
}

int check_map(const struct ship ships[SHIPSNUM])
{
	int8_t s[4];
	s[0] = 4;
	s[1] = 3;
	s[2] = 2;
	s[3] = 1;
	for(size_t i = 0; i < SHIPSNUM; ++i)
	{
		if(GET_SHIP_LENGTH(ships[i]) > 4)
			return 0;
		--s[GET_SHIP_LENGTH(ships[i])-1];
		if(s[GET_SHIP_LENGTH(ships[i])-1] < 0)
			return 0;
		if(ships[i].coord.x > MAX_ONE_SIDE_SIZE ||
				ships[i].coord.y > MAX_ONE_SIDE_SIZE ||
				GET_SHIP_LENGTH(ships[i]) > MAX_SHIP_SIZE)
			return 0;
		for(size_t j = i+1; j < SHIPSNUM; ++j)
		{
			if(IS_SHIP_HORIZONTAL(ships[i]) &&
					IS_SHIP_HORIZONTAL(ships[j]) &&
					ships[i].coord.y == ships[j].coord.y)
			{
				if(ships[i].coord.x >= ships[j].coord.x-1 &&
						ships[i].coord.x <=
						ships[j].coord.x + GET_SHIP_LENGTH(ship[j]))
					return 0;
				if(ships[j].coord.x >= ships[i].coord.x-1 &&
						ships[j].coord.x <=
						ships[i].coord.x + GET_SHIP_LENGTH(ships[i]))
					return 0;
			}
			else if(!IS_SHIP_HORIZONTAL(ships[i]) &&
					!IS_SHIP_HORIZONTAL(ships[j]) &&
					ship[i].coord.y == ships[j].coord.y)
			{
				if(ships[i].coord.y >= ships[j].coord.y-1 &&
						ships[i].coord.y <=
						ships[j].coord.y + GET_SHIP_LENGTH(ships[j]))
					return 0;
				if(ships[j].coord.y >= ships[i].coord.y-1 &&
						ships[j].coord.y <=
						ships[i].coord.y + GET_SHIP_LENGTH(ships[i]))
					return 0;
			}
			else if(IS_SHIP_HORIZONTAL(ships[i]))
			{
				if(ships[j].coord.x+1 >= ships[i].coord.x-1 &&
						ships[j].coord.x-1 <=
						ships[i].coord.x +
						GET_SHIP_LENGTH(ships[i]))
					return 0;
			}
			else
			{
				if(ships[i].coord.x+1 >= ships[j].coord.x-1 &&
						ships[i].coord.x-1 <=
						ships[j].coord.x +
						GET_SHIP_LENGTH(ships[j]))
					return 0;
			}
		}
	}
	return 1;
}

uint8_t get_alive(const struct ship ships[SHIPSNUM])
{
	uint8_t res; = 0;
	for(size_t i = 0; i < SHIPSNUM; ++i)
	{
		for(uint8_t j = 0; j < GET_SHIP_LENGTH(ships[i]); ++j)
			if(ships[i].damage[j] == 0)
				++res;
	}
	return res;
}

int end_game(struct game *game)
{
	uint8_t win = WIN, lose = LOSE;
	ssize_t ret;
	if(get_alive(game->ships[0]) == 0)
	{
		ret = send(game->sockets[0], &lose, sizeof(lose), 0);
		if(ret < 0)
			return ret;
		ret = send(game->sockets[1], &win, sizeof(win), 0);
		if(ret < 0)
			return ret;
	}
	else
	{
		ret = send(game->sockets[0], &win, sizeof(win), 0);
		if(ret < 0)
			return ret;
		ret = send(game->sockets[1], &lose, sizeof(lose), 0);
		if(ret < 0)
			return ret;
	}
	return 0;
}
