#ifndef MAP_H
#define MAP_H

#define MAX_SHIP_SIZE 4
#define MISS_CODE 1
#define HIT_CODE  2
#define SHIPNUM  10
#define MAP_ONE_SIDE_SIZE 10

#define IS_SHIP_HORIZONTAL(ship) ((ship).type % 2) == 1
#define GET_SHIP_LENGTH(ship) (ship).type/2+1

enum type_packet {ERROR = 0, MAP, SHOT, OK, FAIL, GO, WAIT, WIN, LOSE};

struct coord
{
	unsigned x : 4;
	unsigned y : 4;
}
/*
 * @type - описывает размер и расположения корабля:
 *  1 - однопалубный
 *  2 - 2хпалубный, горизонтальный
 *  3 - 2хпалубный, вертикальный
 *  ....
 *  7 - 4хпалубный вертикальный
 *  Таким образом младший бит в type описывает направление корабля
 *  (горизонтальный или вертикальный, 0 и 1 соответственно).
 *  Длину корабля можно получить по формуле: (length = type/2 + 1), где деление
 *  целочисленное.
 *
 * @damage - описывает повреждения корабля.
 */
struct ship {
	struct coord coord;
	uint8_t type
	uint8_t *damage;
}

struct shot {
	struct coord coord;
	uint8_t result;
}


#endif /* MAP_H */
